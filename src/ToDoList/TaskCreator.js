import React, {Component} from "react"
import {Modal, Button, Form} from "semantic-ui-react"

class TaskCreator extends Component{
  constructor(props) {
    super(props);

    this.state = {
        title: '',
        date: '',
        time: '',
        description: '',
        id: '',
        isDone: '',

    }
    this.newIndex = 1;


  }

  handleInput = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    this.setState({
          [name]: value
    });
  }

  handleFormSubmit = (e) => {
    e.preventDefault();
    const newTask = {
      title: this.state.title,
      date: this.state.date,
      time: this.state.time,
      description: this.state.description,
      id: this.newIndex,
      isDone: false
    }

    this.newIndex++;
    this.props.onCreate(newTask);
    console.log(newTask);

    this.setState({
      title: '',
      date: '',
      time: '',
      description: '',
      id: '',
      isDone: ''
    });
  }

  render() {
    const {title, date, time, description} = this.state;

    return (
      <Modal trigger={<Button>+</Button>}>
          <Modal.Header>Create New Task</Modal.Header>
            <Modal.Description>
              <div className="form">
                <Form onSubmit={this.handleFormSubmit}>
                  <Form.Input required
                           placeholder="Title"
                           value = {title}
                           label = "Title"
                           name="title"
                           onChange={this.handleInput} />
                  <Form.Group widths="equal">
                  <Form.Input required
                           value = {date}
                           label = "Date"
                           name="date"
                           type = "date"
                           onChange={this.handleInput} />
                  <Form.Input required
                           value = {time}
                           label = "Time"
                           name= "time"
                           type= "time"
                           onChange={this.handleInput} />
                  </Form.Group>
                  <Form.TextArea
                           label="Description"
                           name="description"
                           value = {description}
                           required
                           onChange={this.handleInput}/>
                  <Form.Button type="submit">Create</Form.Button>
                </Form>
              </div>
            </Modal.Description>
        </Modal>
    )
  }
}

export default TaskCreator
