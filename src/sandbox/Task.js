import React, {Component} from 'react'

class Task extends Component {
  constructor(props ) {
    super(props);


    this.parentDeleteCallback = props.deleteCallback;
    this.parrentUpdateCallback = props.updateCallback;
  }

  deleteTask(e) {
    this.parentDeleteCallback(this.props.task.id);
  }

  toggleTaskStatus(e) {
    const task = {
      ...this.props.task
    };
    task.isDone = !task.isDone;

    this.parrentUpdateCallback(task);
  }

  render() {
    return (
      <div className={ this.props.task.isDone ? 'task done' : 'task'}>
          <input type="checkbox" onClick={this.toggleTaskStatus.bind(this)}/>
          {this.props.task.title}
          <span className="delete" onClick={this.deleteTask.bind(this)}>x</span>
      </div>
    );
  }
}

export default Task
