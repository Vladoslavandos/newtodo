import React, {Component} from "react"
import TaskCreator from "./TaskCreator"
import TaskList from "./TaskList"
import Footer from "./Footer"

class ToDoList  extends Component {
  constructor() {
    super();

    this.state = {
      tasks: [],
      filter: "all"
    }
  }

  createNewTask = (task) => {
    this.setState({
      tasks:[...this.state.tasks, task]
    });
  }

  updateTask = (task) => {
    const newTaskList = [...this.state.tasks];

    newTaskList.forEach((t) => {
      if(t.id === task.id) {
        t.isDone = task.isDone;
        return;
      }
    })

    this.setState({
      tasks: newTaskList
    });
  }

  deleteTask = (taskId) => {
    const newTaskList = this.state.tasks.filter((t) => {
      return t.id !== taskId
    });

    this.setState({
      tasks: newTaskList
    });
  }

  changeFilter = (filterValue) => {
    this.setState({
      filter: filterValue
    });
  }

  clearCompleted = (e) => {
    this.setState({
      tasks: this.state.tasks.filter(t => !t.isDone)
    })
  }

  render() {
    let filteredTasks = [];
    if (this.state.filter === 'all') filteredTasks = this.state.tasks;
    if (this.state.filter === 'active') filteredTasks = this.state.tasks.filter(t => !t.isDone);
    if (this.state.filter === 'completed') filteredTasks = this.state.tasks.filter(t => t.isDone);

    return (
      <div className="app">
        <div className="todolist-header">
          <div className="logo">
            <h1>LIST</h1>
          </div>

          <div className="buttonAddTask">
            <TaskCreator onCreate={this.createNewTask} />
          </div>
        </div>

        <div className="tasklist">
          <TaskList tasks={filteredTasks}
                    onUpdate={this.updateTask}
                    onDelete={this.deleteTask}/>
          </div>

        <Footer tasks={this.state.tasks}
                filter={this.state.filter}
                onChangeFilter={this.changeFilter}
                onClearCompleted={this.clearCompleted}/>

      </div>
    )
  }

}

export default ToDoList
