import React, {Component} from 'react'

import Task from "./Task"


class TaskList extends Component {

  constructor(props) {

    super(props);

  }


  render() {
    return (
        <div className="tasks">
          {
            this.props.task.map((task) => {
              return <Task task={task}
                            updateCallback={this.props.onUpdate}
                            deleteCallback={this.props.onDelete}
                            key={task.id}/>
            })
          }
        </div>

    )
  }
}

export default TaskList
