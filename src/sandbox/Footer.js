import React, {Component} from 'react'

class TodoListFooter extends Component {

  handleFilterChanged(e) {
    this.props.onChangeFilter(e.currentTarget.dataset.value)
  }

  render() {
    return (
      <div className="todolistfooter">
        <div>
          <span>{this.props.task.filter((t) => !t.isDone).length} items left</span>
        </div>
        <div className="buttons">
          <button className={this.props.filter == 'all' ? 'selected' : ''} data-value="all"
                  onClick={this.handleFilterChanged.bind(this)}>ALL</button>
          <button className={this.props.filter == 'active' ? 'selected' : ''} data-value="active"
                  onClick={this.handleFilterChanged.bind(this)}>ACTIVE</button>
          <button className={this.props.filter == 'completed' ? 'selected' : ''} data-value="completed"
                  onClick={this.handleFilterChanged.bind(this)}>COMPLETED</button>
        </div>
        <div>
          <span onClick={this.props.clearCompleted}>CLEAR COMPLETED</span>
        </div>
      </div>
    );
  }
}


export default TodoListFooter
