import React from "react"
import {Button, Table} from "semantic-ui-react"



const TableHeader = () => {
  return (
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>Name</Table.HeaderCell>
        <Table.HeaderCell>Height</Table.HeaderCell>
        <Table.HeaderCell>Mass</Table.HeaderCell>
        <Table.HeaderCell>Gender</Table.HeaderCell>
        <Table.HeaderCell>Birth year</Table.HeaderCell>
      </Table.Row>
    </Table.Header>
  )
}

const TableBody = (props) => {
  const rows = props.people.map((row,index) => {
    return (
    <Table.Row key={index}>
      <Table.Cell>{row.name}</Table.Cell>
      <Table.Cell>{row.height}</Table.Cell>
      <Table.Cell>{row.mass}</Table.Cell>
      <Table.Cell>{row.gender}</Table.Cell>
      <Table.Cell>{row.birth_year}</Table.Cell>
    </Table.Row>
  )
  })

  return <Table.Body>{rows}</Table.Body>
}

class People extends React.Component {
  constructor(){
    super();

    this.state = {
      people: undefined
    };
  }

  gettingPeople = async (e) => {
    e.preventDefault();
    const api_url = await
    fetch(`https://swapi.co/api/people/`)
      .then(response => response.json())
      .then(data => {
        console.log('data', data);
        this.setState({people: data.results});
      })
      .catch(error => console.log('error', error));
  }



  render() {
    return (
      <div className="peopleList">
        <Button onClick={this.gettingPeople}>Получить данные</Button>
        { this.state.people &&
          <Table celled>
            <TableHeader />
            <TableBody people={this.state.people} />
          </Table>
        }
      </div>
    )
  }
}

export default People
