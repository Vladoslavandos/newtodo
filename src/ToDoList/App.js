import React, {Component} from "react"
import ToDoList from "./ToDoList"
import People from "./People/People"
import {Route, Link} from "react-router-dom"
import "./ToDoList.css"
import {
  Menu,
  Sidebar,
  Icon,
  Segment} from "semantic-ui-react"


class App extends Component {
 constructor() {
    super();
    this.state = {
      visible: false
    };
  }


  handleSidebarHide = () => this.setState({ visible: false});
  handleSidebarClick = () => this.setState({ visible: !this.state.visible });



  render() {
    const {visible} = this.state;

    return (
      <div className = "menu">
        <Menu attached="top">
          <Menu.Item name="Menu" onClick={this.handleSidebarClick}>
            <i className="bars icon" />
            <span>Menu</span>
          </Menu.Item>
        </Menu>

        <Sidebar.Pushable as={Segment}>
          <Sidebar
            as={Menu}
            animation='push'
            icon='labeled'
            inverted
            vertical
            visible={visible}
            width='thin'
          >
              <Link to="/">
                <Menu.Item  as='a'>
                  <Icon name='tasks' />
                  ToDoList
                </Menu.Item>
              </Link>

            <Link to="/people">
              <Menu.Item as='a'>
                <Icon name='users' />
                People
              </Menu.Item>
            </Link>

          </Sidebar>

          <Sidebar.Pusher onClick={this.handleSidebarHide}>
              <Segment>
                <Route exact path="/" component={ToDoList} />
                <Route path="/people" component={People} />
              </Segment>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>


    )
  }
}

export default App
