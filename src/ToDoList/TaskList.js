import React, {Component} from "react"
import Task from "./Task"
class TaskList extends Component {

  render() {
    return(
      <div className="Tasks">
      {
        this.props.tasks.map((task) => {
            return <Task task={task}
                         onUpdate={this.props.onUpdate}
                         onDelete={this.props.onDelete}
                         key={task.id}/>
        })
      }
      </div>
    )
  }
}

export default TaskList
