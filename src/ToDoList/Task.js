import React, {Component} from "react"
import {Checkbox, Icon, Card} from "semantic-ui-react"

class Task extends Component {

  toggleTaskStatus = (e) => {
    const task = {...this.props.task};
    task.isDone = !task.isDone;
    this.props.onUpdate(task);
  }

  deleteTask = (e) => {
    this.props.onDelete(this.props.task.id);
  }

  render() {

    const {title, date, time, description, isDone} = this.props.task;

    return (
      <div className={isDone ? "Task done": "Task"}>
        <Card>
          <Card.Content>
            <Card.Header> <Checkbox onChange={this.toggleTaskStatus} checked={isDone}/> <span className="title">{title}</span> <i className="trash icon" onClick={this.deleteTask}/></Card.Header>
            <Card.Meta> {time} | {date} </Card.Meta>
            <Card.Description> {description} </Card.Description>
          </Card.Content>
        </Card>
      </div>
    )
  }
}

export default Task
