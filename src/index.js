import React, {Component} from "react"
import ReactDOM from "react-dom"
import App from "./ToDoList/App"
import {BrowserRouter} from "react-router-dom"
import "semantic-ui-css/semantic.min.css"

ReactDOM.render (<BrowserRouter> <App/> </BrowserRouter>, document.getElementById('root'))
