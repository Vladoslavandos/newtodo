import React, {Component} from "react"
import {Button} from "semantic-ui-react"

class Footer extends Component {

  handleFilterChanged = (e) => {
    this.props.onChangeFilter(e.currentTarget.dataset.value);
  }

  render() {
    return (
      <div className = "footer">
        <div>
          <span>{this.props.tasks.filter((t) => !t.isDone).length} items left</span>
        </div>
        <div className="buttons">
          <Button className={this.props.filter === 'all' ? 'selected' : ''}
                  data-value="all"
                  onClick={this.handleFilterChanged}>ALL</Button>
          <Button className={this.props.filter === 'active' ? 'selected' : ''}
                  data-value="active"
                  onClick={this.handleFilterChanged}>ACTIVE</Button>
          <Button className={this.props.filter === 'completed' ? 'selected' : ''}
                  data-value="completed"
                  onClick={this.handleFilterChanged}>COMPLETED</Button>
          <Button onClick={this.props.onClearCompleted}>CLEAR COMPLETED</Button>
        </div>
      </div>
    )
  }
}

export default Footer
