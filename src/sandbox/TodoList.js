import React, {Component} from 'react'
import "./ToDoList.css"
import Task from "./Task"
import TodoListFooter from "./Footer"
import TaskCreator from "./TaskCreator"
import TaskList from "./TaskList"

class TodoList extends Component {

  constructor() {
    super();



    this.state = {
        task: [
          {
            title: "learn js",
            isDone: false,
            id: 0
          },
          {
            title: "learn react",
            isDone: false,
            id: 1
          }
        ],
        filter: "all"
    };
  }

  createNewTask(task) {

      this.setState({
        task: [...this.state.task, task]
      });
  }


  deleteTask(taskId) {

    const newTaskList = this.state.task.filter((t)=> {
      return t.id !== taskId;
    });

    this.setState({
      task: newTaskList
    });
  }

  updateTask(task) {
    const newTaskList = [...this.state.task]

    newTaskList.forEach((t)=> {
      if(t.id === task.id) {
        t.isDone = task.isDone;
        return;
      }
    })

    this.setState({
      task: newTaskList
    });
  }

  changeFilter(filterValue) {
    this.setState({filter: filterValue});
  }

  clearCompleted() {
    this.setState({
      task: this.state.task.filter(t => !t.isDone)
    });
  }

  render() {
    let filteredTasks = [];
    if (this.state.filter === 'all') filteredTasks = this.state.task;
    if (this.state.filter === 'active') filteredTasks = this.state.task.filter(t => !t.isDone);
    if (this.state.filter === 'completed') filteredTasks = this.state.task.filter(t => t.isDone);

    return (
      <div className="toDoList">
        <TaskCreator onCreate={this.createNewTask.bind(this)}/>

        <TaskList task={filteredTasks}
                  onDelete={this.deleteTask.bind(this)}
                  onUpdate={this.updateTask.bind(this)}/>

        <TodoListFooter task={this.state.task} filter={this.state.filter}
                        onChangeFilter={this.changeFilter.bind(this)}
                        clearCompleted={this.clearCompleted.bind(this)}/>
      </div>
    )
  }
}

export default TodoList
